using System.Data.Entity;

namespace Driwlo.Models;

public class MovieDBContext: DbContext
{
    public DbSet<Movie> Movies { get; set; }
}